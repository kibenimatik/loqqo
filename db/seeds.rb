Bear.delete_all
Bull.delete_all

%w( Jupiter Teddy Bear Hamilton Berny Jammy Arther Mathe Orso Bemot ).each do |n|
  Bear.create!(name: n)
end

%w( Sunset Bambi Boris Skipper Cruiser Darnell Donny Forest Kronk Nicodemus ).each do |n|
  Bull.create!(name: n)
end
