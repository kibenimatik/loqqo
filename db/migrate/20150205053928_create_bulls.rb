class CreateBulls < ActiveRecord::Migration
  def change
    create_table :bulls do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
