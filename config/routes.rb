Rails.application.routes.draw do
  get 'bears' => 'application#bears', as: 'bears'
  get 'bulls' => 'application#bulls', as: 'bulls'
  root to: 'application#index'
end
